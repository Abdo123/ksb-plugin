package com.example.gisro.ksbtest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.EditTextPreference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.brother.ptouch.sdk.LabelInfo;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Printer printer = new Printer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToNetPtrinters(View view){
        Intent intent = new Intent (this, Activity_NetPrinterList.class);
        startActivityForResult(intent, 10007);
    }

    /**
     * Called when the searching printers activity you launched exits.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (10007 == requestCode) {
            int nul = 0;
            if (resultCode == RESULT_OK) {
                ArrayList<String> printerData = new ArrayList<>();
                String ipAddress = data.getStringExtra("ipAddress");
                printerData.add(ipAddress);

                // MAC address
                String macAddress = data.getStringExtra("macAddress");
                printerData.add(macAddress);


                // Printer name
                printerData.add((data.getStringExtra("printer")));


                PrinterInfo printInfo = new PrinterInfo();
                printInfo.printerModel = PrinterInfo.Model.QL_820NWB;
                printInfo.port = PrinterInfo.Port.NET;
                printInfo.ipAddress = ipAddress;
                printInfo.macAddress = macAddress;
                printInfo.labelNameIndex = LabelInfo.QL700.W29H90.ordinal();
                printer.setPrinterInfo(printInfo);

            }
        }

    }




    public void btnForPrint(View view){
        int een = 0;
        Thread trd = new Thread(new Runnable(){

            @Override
            public void run() {
                printer.startCommunication();
                PrinterStatus status = printer.printFile("/storage/emulated/0/Download/woonbedrijf-wal-03.jpg");

                printer.endCommunication();
            }
        });

        trd.start();

    }


}
